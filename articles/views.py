from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from . import models
# Create your views here.

class ArticleListView(LoginRequiredMixin, ListView): #altered
    model = models.Article
    template_name = 'article_list.html'
    login_url = 'login' #added

class ArticleDetailView(DetailView): #altered
    model = models.Article
    template_name = 'article_detail.html'
    login_url = 'login' #added

class ArticleUpdateView(LoginRequiredMixin, UpdateView): #altered
    model = models.Article
    template_name = 'article_edit.html'
    fields = ['title','body',]
    login_url = 'login' #added

class ArticleDeleteView(LoginRequiredMixin, DeleteView): #altered
    model = models.Article
    template_name = 'article_delete.html'
    success_url = reverse_lazy('article_list')
    login_url = 'login' #added

class ArticleCreateView(LoginRequiredMixin, CreateView): #altered
    model = models.Article
    template_name = 'article_new.html'
    fields = ['title', 'body',] #author removed for autofill
    login_url = 'login' #added

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)
